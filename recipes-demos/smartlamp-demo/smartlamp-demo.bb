SUMMARY = "Minimal Smart Lamp Demo"
DESCRIPTION = "A simple Smart Lamp demo"
AUTHOR = "alin.popa.ext@huawei.com"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=7388a7144776640b5c29ecbfa8358b44"

SRC_URI += "\
    git://gitlab.com/harmonyos-hackathon/resources/smart-lamp-demo.git;protocol=https;branch=main; \
    file://smartlamp-demo.service \
    "

SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

DEPENDS = "mqtt-manager \
           libgpiod"

inherit deploy meson pkgconfig systemd
EXTRA_OEMESON = "-DHILINK=false -DTUYA=false"
SYSTEMD_SERVICE_${PN} = "smartlamp-demo.service"

do_install_append() {
    rm -rf "${D}/etc/systemd/system/smartlamp.service"
    
    install -d 0744 "${D}/etc/systemd/system"
    install -m 0644 "${WORKDIR}/smartlamp-demo.service" "${D}/etc/systemd/system/smartlamp-demo.service"
}

FILES_${PN} += "/usr/share/licenses/smart-lamp-demo \
                /usr/share/licenses/smart-lamp-demo/LICENSE \
                /etc/systemd \
                /etc/systemd/system \
                /etc/systemd/system/smartlamp-demo.service \
               "
