SUMMARY = "Minimal MQTT Manager"
DESCRIPTION = "Service to handle application state in MQTT broker using libmosquitto"
AUTHOR = "alin.popa.ext@huawei.com"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=7388a7144776640b5c29ecbfa8358b44"

SRC_URI += "\
    git://gitlab.com/harmonyos-hackathon/resources/mqtt-manager.git;protocol=https;branch=main; \
    file://mqtt-manager.service \
    "

SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

DEPENDS = "glib-2.0 \
           mosquitto \
           systemd"

inherit deploy cmake pkgconfig systemd
SYSTEMD_SERVICE_${PN} = "mqtt-manager.service"

do_install_append() {
    install -d 0744 "${D}/usr/share/licenses/mqtt-manager"
    install -m 0644 "${WORKDIR}/git/LICENSE" "${D}/usr/share/licenses/mqtt-manager/LICENSE"
    
    rm -rf "${D}/etc/systemd/system/mqttmanager.service"
    
    install -d 0744 "${D}/etc/systemd/system"
    install -m 0644 "${WORKDIR}/mqtt-manager.service" "${D}/etc/systemd/system/mqtt-manager.service"
}

FILES_${PN} += "/usr/share/licenses/mqtt-manager \
                /usr/share/licenses/mqtt-manager/LICENSE \
                /etc/systemd \
                /etc/systemd/system \
                /etc/systemd/system/mqtt-manager.service \
               "
