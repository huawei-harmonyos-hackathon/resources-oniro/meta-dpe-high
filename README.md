This README file contains information on the contents of the meta-dpe-high layer.    
Please see the corresponding sections below for details.

## Dependencies
This meta-layer is used and tested with Oniro. Please see the official documentation for more details: https://docs.oniroproject.org/en/latest/overview/index.html

* URI: https://booting.oniroproject.org/distro/oniro    
* branch: dunfell    

## Patches
Please submit any patches against the meta-dpe-high layer please create a merge request for this project
* URI: https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-high    
* Maintainer: Alin Popa (alin.popa.ext@huawei.com)

## Table of Contents
I. Adding the meta-dpe-high layer to your build    
II. Build a demo

### I. Adding the meta-dpe-high layer to your build
1. Please follow the instructions from local manifests      
https://gitlab.com/harmonyos-hackathon/resources-oniro/local-manifests/-/blob/main/README.md

2. Source the Linux flavour
```
# TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-linux
```

3. Add the layer in your layer config
```
# vim conf/bblayers.conf
# <add meta-dpe-high as any other layers from sources directory>
```

### II. Build a demo
A demo target can be built by specifying the MACHINE and adding the needed recipes to the target.    
Eg. Smart Lamp Demo: https://gitlab.com/harmonyos-hackathon/resources-oniro/smart-lamp-demo

```
# MACHINE=raspberrypi4-64 bitbake oniro-image-base
```

